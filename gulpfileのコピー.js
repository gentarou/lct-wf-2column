var conf = {
    port: 8080,
    uiPort: 8081
};

var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();

gulp.task('uglify', () => {
    gulp.src(['js/**/*.js', '!js/**/*.min.js'])
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('js/'));
});

gulp.task('cleanCSS', () => {
    gulp.src(['css/**/*.css', '!css/**/*.min.css'])
        .pipe(cleanCSS())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('css/'));
});

gulp.task('minify', ['uglify', 'cleanCSS']);

gulp.task('browserSync', () => {
    browserSync.init({
        port: conf.port,
        ui: {
            port: conf.uiPort
        },
        server: './',
        index:'index.html'
    });
});

gulp.task('watch-html', ['browserSync'], () => {
    gulp.watch(['**/*.html'], () => {
        gulp.src('**/*.html')
            .pipe(browserSync.reload({
                stream: true
            }));
    });
});

gulp.task('watch-js', ['browserSync'], () => {
    gulp.watch(['js/**/*.js'], () => {
        gulp.src('js/**/*.js')
            .pipe(browserSync.reload({
                stream: true
            }));
    });
});

gulp.task('watch-sass', ['browserSync'], () => {
    gulp.watch(['assets/sass/**/*.sass'], () => {
        gulp.src('assets/sass/**/*.sass')
            .pipe(plumber())
            .pipe(sass({
                outputStyle: 'compact'
            }))
            .pipe(gulp.dest('./assets/css/'))
            .pipe(browserSync.reload({
                stream: true
            }));
    });
});

gulp.task('watch-scss', ['browserSync'], () => {
    gulp.watch(['assets/sass/**/*.scss'], () => {
        gulp.src('assets/sass/**/*.scss')
            .pipe(plumber())
            .pipe(sass({
                outputStyle: 'expanded'
            }))
            .pipe(gulp.dest('./assets/css/'))
            .pipe(browserSync.reload({
                stream: true
            }));
    });
});

gulp.task('watch', ['watch-html', 'watch-js', 'watch-sass', 'watch-scss']);

gulp.task('default', ['watch']);
