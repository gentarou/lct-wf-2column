
# lct-html-format
Ver.1.1.0 (2019.11.26)

（現状のOS環境 
node：v8.9.4  
gulp：[12:58:54] CLI version: 2.2.0
      [12:58:54] Local version 3.9.1
sass：Ruby Sass 3.6.0）

●以下の手順で使用してください。

・プルしたフォルダを作業するディレクトリに格納
・ターミナルでそのディレクトリへ移動
・$ npm init を実行
・$ npm install を実行
・$ gulp 実行

でブラウザが立ち上がると思うのでディレクトリをエディターで開いて作業してください。

