(function($){
$(function(){
    $("#gnav").mmenu({
        extensions: [
            "shadow-page",
            "border-full",
            "pagedim-black"
        ],
        navbar : false,
        offCanvas: {
            position: "right"
        },
        slidingSubmenus: false,
    });
    var API = $("#gnav").data( "mmenu" );
    $("#gnav-open").click(function() { API.open(); });
    $("#gnav-close").click(function() { API.close(); });
});
})(jQuery);